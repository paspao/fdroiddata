Application officielle Wikipédia pour Android. Wikipédia est l’encyclopédie libre contenant plus de 32 millions d’articles en 280 langues et est la référence la plus complète et la plus largement utilisée que les hommes aient jamais compilée. L’application a été réécrite de zéro et est meilleure que jamais.

Fonctionnalités :
   - Vitesse : notre nouvelle application native vous permet de naviguer sur Wikipédia et de modifier plus vite que jamais.
   - Modification : vous pouvez modifier Wikipédia ! connecté ou non, nous vous remercions pour toutes vos contributions.
   - Pages récentes : nous conservons votre historique de lecture, afin que vous puissiez suivre autant de liens que vous voulez sans jamais être perdu.
   - Pages enregistrées : vous pouvez enregistrer des pages pour une lecture hors ligne et y naviguer même sans avoir de connexion.
   - Partager : utilisez vos applications de réseau social existantes pour partager la somme de tout le savoir humain.
   - Support linguistique : l’application vous permet de basculer de façon transparente pour lire Wikipédia dans n’importe quelle langue.
   - Wikipédia Zéro : les frais de données sont annulés pour les opérateurs mobiles participants.

Le code est entièrement ouvert. Si vous êtes expérimenté avec Java et le kit de développement Android, alors venez créer votre variante ! https://github.com/wikimedia/apps-android-wikipedia

Recherche par la voix intégré - Trouvez facilement ce que vous cherchez avec une grande barre de recherche en haut de l'application, incluant l'activation de la recherche vocale sur votre appareil

Proche - Apprenez plus sur ce qu'il y a autour de vous en sélectionnant des points sur une carte interactive pour voir les articles associés à votre position actuelle et proche.

Prise en charge linguistique - Changez facilement à la lecture de n’importe quelle langue de Wikipédia, soit en changeant la langue de l’article courant, soit en changeant votre langue préférée lors d’une recherche.

Aperçu des liens - Tapez sur un article pour avoir un aperçu, sans perdre l'endroit où vous lisiez. Appuyez et maintenez un lien pour l'ouvrir dans un nouvel onglet, vous permettant de continuer à lire votre article sans perdre l'endroit où vous lisiez, et passez au nouvel onglet dès que vous être prêt.

Table des matières - Glissez votre doigt à gauche sur n'importe quel article pour faire apparaître la table des matières, qui vous permettra de passer d'une section d'article à une autre facilement.

Listes de lecture - Organisez les articles que vous trouvez dans des listes de lectures, que vous pourrez accéder même en hors-ligne. Créez autant de listes que vous voulez, donnez leur des noms personnalisés et des descriptions, et remplissez les avec les articles de wiki de n'importe quelle langue.

Galerie d'images - Tapez sur une image pour la voir en plein-écran et en haute résolution, avec des options via glissement du doigt pour rechercher d'autres images.

Définitions de Wikitionnaire - Tapez-et-maintenez pour surligner un mot, puis tapez le bouton "Définir" pour voir une définition du mot du Wikitionnaire.

Wikipedia Zero - Accéder à Wikipedia sur Android sans frais avec les opérateurs mobile participants. (liens des opérateurs participants).

Envoyez-nous des retours à propos de l'application ! Dans le menu, appuyez sur "Paramètres", puis "À propos de l'application Wikipédia", enfin "Envoyer un avis sur l'application".

Le code est 100 % open-source. Si vous avez de l'expérience avec Java et le SDK Android, alors nous attendons avec impatience vos contributions ! https://github.com/wikimedia/apps-android-wikipedia

En utilisant cette application vous acceptez la transmission automatique de rapports d'incident à un fournisseur de service tiers. Si vous voulez désactiver cette fonctionnalité, veuillez appuyer sur "Paramètres", puis désactiver "Envoyer des rapports d'incident" en-dessous de la section Général. Explications des permissions nécessaires par l'applic