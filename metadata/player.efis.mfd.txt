Categories:Navigation
License:Apache-2.0
Web Site:http://members.iinet.net.au/~ninelima/efis/
Source Code:https://github.com/ninelima/kwikEFIS
Issue Tracker:https://github.com/ninelima/kwikEFIS/issues
Changelog:https://github.com/ninelima/kwikEFIS/blob/HEAD/CHANGELOG.md
Bitcoin:1KKWRF25NwVgNdankr1vBphtkLbX766Ee1

Auto Name:Kwik DMAP
Summary:Electronic Flight Information System (MMAP)
Description:
Kwik Moving Map is a companion application to the EFIS. It provides  situational
awareness and and navigation functionality.

'''Synthetic Vision'''

Synthetic vision is built from the USGS 3 arc second Digital Elevation Model
data. If enabled, it also allows for some terrain collision detection. The
required terrain data packages are available on F-Droid, see:

1. [[player.efis.data.usa.can]] North America/Canada

2. [[player.efis.data.eur.rus]] Europe/Russia

3. [[player.efis.data.sah.jap]] Sahara/Northern Africa/India/Indonesia/Japan

4. [[player.efis.data.zar.aus]] Southern Africa/Australia

'''Airspace'''

Airspace data is provided from Worldwide Soaring Turnpoint Exchange Airspace
[http://soaringweb.org/Airspace/]

1. North West Sector: United States/Canada.

2. North East Sector: Europe(Denmark, Germany, Switzerland)/Russia.

3. South West Sector: None.

4. South East Sector: Australia/South Africa.

'''Demo Mode'''

There is a "Demo Mode" available in the application. It is fairly basic and
works like a crude flight simulator. The heading can be changed by banking the
device. The speed is fixed at around 200 kts.

'''Primary Flight Display'''

Kwik EFIS [[player.efis.pfd]] is a companion application to the moving map. It
provides a Primary Flight Display functionality.

'''Screenshots''' [http://members.iinet.net.au/~ninelima/efis/gallery.html]
.

Repo Type:git
Repo:https://github.com/ninelima/kwikEFIS

Build:1.0,1
    commit=VER_3.6
    subdir=mfd
    gradle=yes

Build:1.1,2
    commit=VER_3.7
    subdir=mfd
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags ^VER
Current Version:1.1
Current Version Code:2
