OsmAnd+ (OSM automatiserte navigeringsveivisninger) er et kart og navigasjonsprogram med tilgang til den frie, verdensomspennende kvalitetstjenesten OpenStreetMap (OSM). Nyt stemme og optisk navigasjon, oversikt over interessepunkter, opprettelse og behandling av GPX-spor, bruk av konturlinjevisualisering og høydeinfo, valget mellom kjøring, sykling og fotgjengeri, OSM-redigering og mye mer.

OsmAnd+ er den betalte versjonen av programmet. Ved å kjøpe den, støtter du prosjektet, bidrar til utviklingen av nye funksjoner, og mottar de siste oppdateringene.

